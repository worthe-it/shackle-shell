# Contributing

## Development Guide

### Development Environment Setup

- Rust
  - This project is built using the Rust programming language, and its build
    tool Cargo. Information on how to install these tools is available on [the
    Rust website](https://www.rust-lang.org/learn/get-started).
- C Compiler
  - This is used to built one of the dependencies, libgit2. This can be
    obtained from your operating system's package manager.
- Docker
  - Some of the tests use Docker to create a simulated environment. This can be
    obtained from your operating system's package manager, or the [Docker
    website](https://www.docker.com/).
  - Docker must be installed, with the `docker` executable on the path.
  - Your user must have permission to use docker, including building and running
    Docker containers.
- Git
  - Some of the tests use Git to test the end to end functionality of the
    shell. This can be obtained from your operating system's package manager, or
    the [Git website](https://git-scm.com/).
  - Git must be installed, with the `git` executable on the path.
- SSH
  - Some of the tests use an SSH client to test the end to end functionality of
    the shell. I have tested this with OpenSSH, which can be obtained from your
    operating system's package manager, or the [Git website](https://git-scm.com/).
  - with the `ssh` executable on the path.

If you're running Arch Linux, these are the steps you'll need to get your
environment ready.

```sh
sudo pacman -S rustup docker git openssh gcc

# Rust
rustup default stable

# Docker
sudo usermod -a -G docker $USER
sudo systemctl start docker.service

# Note: you need to log out and in again for the new group to take effect
```

### Running Tests

All unit tests are run with Cargo.

```sh
cargo test
```

### Linting

Clippy should be used for linting. This can be installed using Rustup.

```sh
rustup component add clippy
```

And it can be run via Cargo.

```sh
cargo clippy
```

### Building a Release Binary

Release binaries should be built in release mode.

```sh
cargo build --release
```

After running this, the binary is available in `./target/release/shackle-shell`

## Submitting contributions

If you have a change you'd like to contribute upstream, please open a pull
request on
[Codeberg](https://codeberg.org/worthe-it/shackle-shell/pulls).

## I Have a Question

If you have a question which isn't answered by any of the existing
documentation, please open an
[issue](https://codeberg.org/worthe-it/shackle-shell/issues).

Please search the issues to see if anyone else has already asked the same
question before opening a new issue.
