mod cli_test_utils;

use anyhow::Result;
use cli_test_utils::*;

const REPO_NAME: &str = "my-repository";

#[test]
fn shows_a_prompt() -> Result<()> {
    TestContext::new_interactive()?;
    Ok(())
}

#[test]
fn does_nothing_after_receiving_whitespace_input() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line("")?;
    c.expect_prompt()?;
    c.p.send_line("  ")?;
    c.expect_prompt()?;
    Ok(())
}

#[test]
fn quits_when_eof_is_sent() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_control('d')?;
    c.p.exp_eof()?;
    Ok(())
}

#[test]
fn quits_when_exit_command_is_sent() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line("exit")?;
    c.p.exp_eof()?;
    Ok(())
}

#[test]
fn reports_error_with_unsupported_shell_commands() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line("ls")?;
    c.p.exp_string("error: unrecognized subcommand 'ls'")?;
    c.expect_prompt()?;
    Ok(())
}

#[test]
fn reports_error_with_nonsense_input() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line(" asd fg  ")?;
    c.p.exp_string("error: unrecognized subcommand 'asd'")?;
    c.expect_prompt()?;
    Ok(())
}

#[test]
fn allows_quotes_arguments() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line(&format!("\"init\" '{REPO_NAME}'"))?;
    c.expect_successful_init_message(&personal_repo_path(REPO_NAME))?;
    Ok(())
}

#[test]
fn errors_with_an_open_double_quote() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line("\"init 'another-new-repo'")?;
    c.p.exp_string("Incomplete input")?;
    Ok(())
}

#[test]
fn errors_with_an_open_single_quote() -> Result<()> {
    let mut c = TestContext::new_interactive()?;
    c.p.send_line("'init 'another-new-repo'")?;
    c.p.exp_string("Incomplete input")?;
    Ok(())
}

#[test]
fn allows_single_quotes_and_spaces_inside_double_quotes() -> Result<()> {
    let repo_name = "shukkie's new repo";
    let mut c = TestContext::new_interactive()?;
    c.p.send_line(&format!("init \"{repo_name}\""))?;
    c.expect_successful_init_message(&personal_repo_path(repo_name))?;
    Ok(())
}

#[test]
fn runs_a_single_command_and_exit_with_cli_flag() -> Result<()> {
    let mut c = TestContext::new_batch(&format!("init {}", REPO_NAME))?;
    c.p.exp_string(&format!(
        "Successfully created \"{}\"",
        personal_repo_path(REPO_NAME)
    ))?;
    c.p.exp_eof()?;
    Ok(())
}
