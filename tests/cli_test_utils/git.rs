use crate::cli_test_utils::context::TestContext;
use anyhow::Result;
use assert_cmd::{assert::Assert, Command};
use std::{
    fs,
    path::{Path, PathBuf},
};

pub fn create_clone(c: &TestContext, repo_dir: &Path, relative_name: &str) -> PathBuf {
    Command::new("git")
        .arg("clone")
        .arg(repo_dir)
        .arg(relative_name)
        .current_dir(&c.workdir)
        .timeout(std::time::Duration::from_secs(3))
        .assert()
        .success();

    let repo_dir = c.workdir.as_ref().join(relative_name);

    // standardising the main branch of our clone simplifies things.
    Command::new("git")
        .args(["checkout", "-b", "main"])
        .current_dir(&repo_dir)
        .assert()
        .success();

    repo_dir
}

pub fn create_commit(repo_dir: &Path) -> Result<String> {
    Command::new("git")
        .args(["config", "user.email", "shukkie@example.com"])
        .current_dir(&repo_dir)
        .assert()
        .success();
    Command::new("git")
        .args(["config", "user.name", "Shackle Test User"])
        .current_dir(&repo_dir)
        .assert()
        .success();

    let test_file_name = repo_dir.join("some_file");
    fs::write(test_file_name, "Some content or something")?;
    Command::new("git")
        .args(["add", "--all"])
        .current_dir(&repo_dir)
        .timeout(std::time::Duration::from_secs(3))
        .assert()
        .success();
    Command::new("git")
        .args(["commit", "-m", "A commit message"])
        .current_dir(&repo_dir)
        .timeout(std::time::Duration::from_secs(3))
        .assert()
        .success();

    let commit_hash = String::from_utf8(
        Command::new("git")
            .args(["rev-parse", "HEAD"])
            .current_dir(&repo_dir)
            .timeout(std::time::Duration::from_secs(3))
            .output()?
            .stdout,
    )?;

    Ok(commit_hash.trim().to_owned())
}

pub fn push(local_repo_dir: &Path, refspec: &str) {
    Command::new("git")
        .args(["push", "origin", refspec])
        .current_dir(&local_repo_dir)
        .timeout(std::time::Duration::from_secs(3))
        .assert()
        .success();
}

pub fn verify_repo_exists(repo_dir: &Path) {
    Command::new("git")
        .arg("rev-list")
        .arg("--all")
        .current_dir(repo_dir)
        .assert()
        .success()
        .stdout("");
}

fn check_commit_exists(repo_dir: &Path, commit_hash: &str) -> Assert {
    Command::new("git")
        .arg("rev-list")
        .arg(commit_hash)
        .current_dir(repo_dir)
        .assert()
}

pub fn verify_commit_exists(repo_dir: &Path, commit_hash: &str) {
    check_commit_exists(repo_dir, commit_hash).success();
}

pub fn verify_commit_does_not_exist(repo_dir: &Path, commit_hash: &str) {
    check_commit_exists(repo_dir, commit_hash).failure();
}

pub fn verify_repo_does_not_exist(repo_dir: &Path) {
    assert!(!repo_dir.exists());
}

pub fn verify_current_branch(repo_dir: &Path, expected_ref: &str) {
    Command::new("git")
        .arg("symbolic-ref")
        .arg("HEAD")
        .current_dir(repo_dir)
        .assert()
        .success()
        .stdout(format!("{expected_ref}\n"));
}

pub fn verify_repo_config_value(repo_dir: &Path, config_key: &str, config_value: Option<&str>) {
    let assert = Command::new("git")
        .args(["config", "--local", config_key])
        .current_dir(repo_dir)
        .assert();
    match config_value {
        Some(value) => {
            assert.success().stdout(format!("{value}\n"));
        }
        None => {
            assert.failure().code(1);
        }
    }
}
