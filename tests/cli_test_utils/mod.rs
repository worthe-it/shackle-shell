// this common code is shared in multiple high level tests, which rust will
// compile as separate binaries. Unfortunately not all of those tests will use
// everything here, which results in spurious dead code warnings.
#![allow(dead_code)]

pub mod context;
pub mod git;

pub use context::*;
pub use git::*;
