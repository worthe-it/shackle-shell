use clap::Parser;
use rustyline::{error::ReadlineError, DefaultEditor};
use shackle_shell::{run_command, ShackleError};

/// Shackle Shell - A replacement for git-shell with repo management commands built in.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Run a single shell command and exit
    #[arg(short, long)]
    command: Option<String>,
}

fn main() {
    if let Err(e) = main_inner() {
        eprintln!("{}", e);
        std::process::exit(1);
    }
}

fn main_inner() -> Result<(), ShackleError> {
    let args = Args::parse();
    match args.command {
        Some(user_input) => {
            run_command(&user_input)?;
        }
        None => {
            run_interactive_loop()?;
        }
    }

    Ok(())
}

fn run_interactive_loop() -> Result<(), ShackleError> {
    let mut rl = DefaultEditor::new()?;
    loop {
        let readline = rl.readline("> ");
        match readline.as_ref().map(|user_input| user_input.trim()) {
            Ok("") => {}
            Ok(user_input) => {
                rl.add_history_entry(user_input)?;
                match run_command(user_input) {
                    Ok(control_flow) => {
                        if control_flow.is_break() {
                            break;
                        }
                    }
                    Err(e) => {
                        println!("{}", e);
                    }
                }
            }
            Err(ReadlineError::Interrupted) => {
                println!("Interrupted");
                break;
            }
            Err(ReadlineError::Eof) => {
                break;
            }
            Err(err) => {
                println!("Error: {}", err);
                break;
            }
        }
    }
    Ok(())
}
