use nix::unistd::{getgroups, getuid, Gid, Group, User};

pub fn get_username() -> Option<String> {
    let uid = getuid();
    User::from_uid(uid).ok().flatten().map(|user| user.name)
}

pub fn get_user_groups() -> Vec<String> {
    let gids = getgroups().unwrap_or(Vec::default());
    gids.into_iter()
        .filter_map(|gid| Group::from_gid(gid).ok().flatten())
        .map(|group| group.name)
        .collect()
}

pub fn get_gid(group_name: &str) -> Option<Gid> {
    nix::unistd::Group::from_name(group_name)
        .ok()
        .flatten()
        .map(|group| group.gid)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_returns_a_username() {
        // We can't get too specific here because this is your actual username,
        // but we at lease expect it to be some string, not None.
        let username = get_username();
        let username_len = username.unwrap().trim().len();
        assert!(username_len > 0);
    }

    #[test]
    fn it_returns_some_groups() {
        let groups = get_user_groups();
        assert!(groups.len() > 0);
        for group in groups {
            assert!(group.trim().len() > 0);
        }
    }
}
