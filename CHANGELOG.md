# Changelog

All notable changes to this project will be documented in this file.

This changelog focusses on changes which affect production users of this
library. It will not mention any changes to code structure, internal design
details, documentation, or testing unless they have some affect on the public
API.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Commands that aren't meant to be used interactively (`git-upload-pack` and
  `git-receive-pack`) no longer appear when you use the `help` command. These
  commands still exist and work as before, so that they can be used by `git
  fetch` and `git push`.

## [0.3.0] - 2023-08-18

### Added

- The license has been updated to also have the option of using the Apache 2.0
  license. This application is now dual licensed with the MIT license, as is
  common with Rust crates.

### Fix

- When a repo is created in a directory which does not exist yet, the directory
  is created with appropriate permissions. For example, when creating a personal
  repo if `git/username` does not exist, it will be created with personal read,
  write and execute access for the user only. If `git/group` does not exist, it
  will be created owned by `group`, with group level read, write and execute
  access, and the setgid flag set.

### Changed

- Updated crates.io metadata to mark the primary upstream repo as Codeberg.
- When deleting a repo, you are now asked first to confirm if you are sure
  you're deleting the right repo.

## [0.2.0] - 2023-07-17

### Added

- Added feature flag "docker_tests". This only affects which tests are run by
  default, so that the CI environment can skip tests that require running
  docker.
- New command "delete", for deleting an existing repo.
- New "--verbose" option to the list command, which also lists the repo size.
- New command "housekeeping", for doing cleaning up unreachable objects in a
  repo and doing object repacking.

## [0.1.1] - 2023-05-10

### Changed

- Updated documentation for installing to reference the package on crates.io.

## [0.1.0] - 2023-05-10

### Added

- Initial release with minimum viable functionality
  - Interactive command prompt
  - Non-interactive commands can be run with -c
  - Exit command
  - Help command
  - Responds appropriately to unknown commands
  - Shell command history (only within same session)
  - git init works for
    - private repos
    - shared repos
  - git fetch / clone from a server using shackle-shell
  - git push to a server using shackle-shell
  - Repos are restricted to only expected paths
  - List all repos
  - Set the repo description, both during init or as a separate command
  - Set the main branch of a repo, both during init or as a separate command

[unreleased]: https://codeberg.org/worthe-it/shackle-shell/compare/v0.3.0...HEAD
[0.3.0]: https://codeberg.org/worthe-it/shackle-shell/releases/tag/v0.3.0
[0.2.0]: https://codeberg.org/worthe-it/shackle-shell/releases/tag/v0.2.0
[0.1.1]: https://codeberg.org/worthe-it/shackle-shell/releases/tag/v0.1.1
[0.1.0]: https://codeberg.org/worthe-it/shackle-shell/releases/tag/v0.1.0
