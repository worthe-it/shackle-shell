FROM debian:bookworm

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
       git openssh-server \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 22

RUN sed -i /etc/ssh/sshd_config \
        -e 's/#PasswordAuthentication.*/PasswordAuthentication yes/' \
        -e 's/#PermitEmptyPasswords.*/PermitEmptyPasswords yes/'

RUN adduser --shell /usr/bin/shackle-shell shukkie && passwd -d shukkie
RUN mkdir -p /home/shukkie/git/shukkie && \
    chown -R shukkie:shukkie /home/shukkie/git
RUN groupadd shukkies-company --users shukkie && \
    mkdir -p /home/shukkie/git/shukkies-company && \
    chmod 2770 /home/shukkie/git/shukkies-company && \
    chown -R root:shukkies-company /home/shukkie/git/shukkies-company
RUN git init --bare /home/shukkie/disallowed.git && \
    chown -R shukkie:shukkie /home/shukkie/disallowed.git
COPY . /opt/shackle

ARG SHELL=target/debug/shackle-shell
RUN cp /opt/shackle/${SHELL} /usr/bin/shackle-shell

ENTRYPOINT service ssh start && echo "Ready" && bash

# docker build -t shackle-server --build-arg SHELL=target/debug/shackle-shell ./
# docker run -it -p 2022:22 shackle-server
# ssh -p 2022 shukkie@localhost