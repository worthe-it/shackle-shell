# Shackle Shell

[![status-badge](https://ci.codeberg.org/api/badges/worthe-it/shackle-shell/status.svg)](https://ci.codeberg.org/worthe-it/shackle-shell)
[![crates.io](https://img.shields.io/crates/v/shackle-shell)](https://crates.io/crates/shackle-shell)

A shell for restricting access on a version control server.

This is intended as a replacement for [Git Shell](https://git-scm.com/docs/git-shell), but with a few opinionated
differences:

- Additional commands, like creating new repos, are built in. No extension with
  shell scripts.
- Strict enforcement of a specified directory structure. Git push / pull
  commands only work for these paths. New repos are created in these paths.
  - Private git repos are created in `~/git/<username>/<reponame>.git`
  - Shared git repos are created in `~/git/<groupname>/<reponame>.git`
- Better interactive UX than Git Shell, including:
  - Command history (limited to the current session).
  - Emacs-style shortcuts (eg `Ctrl+a` to move cursor to the beginning of line).
  - Docs available for all commands, available in the shell itself.
- (coming soon!) Support for other other version control systems.

## Getting Started

### Usage

Shackle Shell is intended to be set as the default shell for a user on a git
server, where the user connects over SSH (see the Installation section below).

When you log in over SSH, you'll see a prompt, ready for you to type commands.

```sh
>
```

You can see the built in help by running the `help` command.

```
> help
Usage: <COMMAND>

Commands:
  init              Create a new repository
  list              List all repositories available
  set-description   Sets the description of a repository, as shown in the CLI listing and web interfaces
  set-branch        Sets the main branch of the repository
  exit              Quit the shell
  git-upload-pack   Server side command required to git fetch from the server
  git-receive-pack  Server side command required by git push to the server
  help              Print this message or the help of the given subcommand(s)

Options:
  -h, --help  Print help
```

The `init` command is used to create a new repo. In its simplest form, you just
provide it with the name of your new repo. This will create a git repo with
individual ownership.

```
> init awesome-project-idea
Successfully created "git/shukkie/awesome-project-idea.git"
```

The path given here is relative to your home directory. So the full URL to clone
this repo is `<username>@<server domain>:<git path>`

```sh
git clone shukkie@example.com:git/shukkie/awesome-project-idea.git
```

You can learn its advanced options by using the `--help` flag. This works for
all of the options, and is a great way to learn what all the commands do.

```
> init --help
Create a new repository

Usage: init [OPTIONS] <REPO_NAME>

Arguments:
  <REPO_NAME>  Name of the new repository

Options:
      --group <GROUP>              Share repository ownership with the specified group (user must be a member of the group)
      --description <DESCRIPTION>  Sets the description of the repository, as shown in the CLI listing and web interfaces
      --branch <BRANCH>            Sets the main branch of the repository [default: main]
  -h, --help                       Print help
```

### Installation

#### Background

Shackle is designed to run on a Git server. The process of setting up your own
Git server, which is set up as described in the following two articles:

- [initial server setup](https://www.worthe-it.co.za/blog/2022-06-10-how-to-train-your-git-server.html)
- [sharing repos](https://www.worthe-it.co.za/blog/2023-06-02-leveling-up-your-git-server-sharing-repos.html).

#### Prerequisites

The following programs much be installed:

- Git
  - This is used for git operations which are passed through for the operation
    of `git push` and `git fetch`.
  - Git must be installed, with the `git-upload-pack` and `git-receive-pack`
    executables on the path.
- SSH
  - I assume users will be connecting over SSH. This is not enforced by the
    shell, but is the primary use case I have in mind. I have tested this using
    the OpenSSH daemon.


#### Building Shackle

Binary releases of Shackle can be downloaded from [Codeberg](https://codeberg.org/worthe-it/shackle-shell/releases/).

You can also build it yourself from source. The easiest way to do this is using
`cargo install`.

This requires a the Rust toolchain and a C compiler. See the Development
Environment Setup section below for more information on environment setup to
build from source.

```sh
# This installs to Cargo's default, which is ~/.cargo/bin. Consider using the
# --root option to install it somewhere that makes more sense for your server.
cargo install shackle-shell
```

#### Creating the required directory structure

Next, Shackle expects a specific directory structure. Specifically, personal git
repos will live in `~/git/your-username` and shared repos will live in
`~/git/your-group`. If you create a repo that requires one of these group
directories and it doesn't exist yet, Shackle will create it.

If you have many users on your server, then consider making `~/git` a symlink to
the actual shared location for your git repos. For example, on my repo, all git
users have a `~/git` symlink in their home directory which actually points at
`/srv/git`.

```sh
mkdir -p ~/git/$USER
chmod --recursive u=rwX,g=,o= ~/git/$USER

# Note that this should be a group you're actually a member of!
GROUP=my-group
mkdir -p ~/git/$GROUP
chown --recursive $USER:$GROUP ~/git/$GROUP
chmod --recursive u=rwX,g=rwXs,o= ~/git/$GROUP
```

#### Set Shackle as your default shell

The next step is to set the default shell of your user to Shackle. This will
mean that if you login as the user, for example over ssh, then the default shell
will be Shackle.

```sh
sudo usermod --shell $HOME/.cargo/bin/shackle-shell $USER
```

## Operating System Support

Currently, Shackle Shell only supports running on Linux.

It will likely work on other Unix-like platforms, such as MacOS, but this is not
currently tested, and there are currently no plans to do so. I would not expect
it to work on Windows.

## Releases

See [CHANGELOG.md](https://codeberg.org/worthe-it/shackle-shell/src/branch/main/CHANGELOG.md)
for release notes.

Releases are also available on
[Crates.io](https://crates.io/crates/shackle-shell) and
[Codeberg](https://codeberg.org/worthe-it/shackle-shell/releases).

## Reporting issues

Please report any issues on the project's [issue
tracker](https://codeberg.org/worthe-it/shackle-shell/issues).


## Roadmap

Planned features are captured in the project's issue tracker with the
["enhancement"
label](https://codeberg.org/worthe-it/shackle-shell/issues?q=&type=all&state=open&labels=119659&milestone=0&assignee=0&poster=0).

The general plans are to

- Fix any bugs which come up.
- Add more commands for configuring repositories.
- Add more commands to do things to repositories, like move them around or do
  housekeeping tasks.
- Add support for other version control systems.

## Contributing

See [CONTRIBUTING.md](https://codeberg.org/worthe-it/shackle-shell/src/branch/main/CONTRIBUTING.md)
for instructions on setting up a development environment to make changes and
submit contributions.

## Support

If you get value from this project, consider supporting me on
[Patreon](https://www.patreon.com/worthe_it). Support via Patreon helps to cover
hosting, buying computer stuff, and will allow me to spend more time writing
articles and open source software.

## License

Copyright 2023, Justin Wernick.

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
